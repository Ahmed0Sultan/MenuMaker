* Run api.py file to perview data retrieved from WidebotAPI and also you can test the pretrained model by accessing the url "/query/string" and replace string with your actual query.

* Run load_data.py file to retrieve data from WidebotAPI and store into [entitiesAPI.wl,intentAPI.wl,queriesAPI.wl,tokensAPI.wl] files and then 

* Run the following command in your terminal to generate the "train.ctf" file python txt2ctf.py --map "tokensAPI.wl" "intentAPI.wl" "entitiesAPI.wl" --annotated False --input "queriesAPI.wl" --output "train.ctf"

* Run Model.py file to create and train the model and save the trained models in the Model Directory.
