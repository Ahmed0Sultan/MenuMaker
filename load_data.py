# coding: utf-8
import sys
import requests

lstm_cells_num = 200
embedding_dim = 100
classify_entents = False
entent_dim = 27
entities_dim = 22
vocab_dim = 463
access_token = "SVHgomCs5K9mOk8bL7As8nwt6RW9GbPoT1o1FImOYNQSAp5CaPAcpaxRds/vH4v20XS9J+ZcbnBEUgSLk5ojM96fKSZ21Y6AsO4RZdWkzfu9Yve/9ePyFQ=="


headers = {'access_token': access_token}
queries_r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_queries', headers=headers)
queries = queries_r.json()
queries_file = open('queriesAPI.wl', "w",encoding='utf-8')
for q in queries:
    queries_file.write(q + '\n')
queries_file.close()
# print(queries)
intents_r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_intents', headers=headers)
intents = intents_r.json()
intents_file = open('intentAPI.wl', "w")
for i in intents:
    intents_file.write(i + '\n')
intents_file.close()
entities_r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_entities', headers=headers)
entities = entities_r.json()
entities_file = open('entitiesAPI.wl', "w")
for e in entities:
    entities_file.write(e+ '\n')
entities_file.close()
tokens_r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_tokens', headers=headers)
tokens = tokens_r.json()
tokens_file = open('tokensAPI.wl', "w",encoding='utf-8')
for t in tokens:
    tokens_file.write(t+ '\n')
tokens_file.close()
