# coding: utf-8
import sys

from flask import Flask, jsonify
import cntk as C
import string
import requests
import pandas as pd
import numpy as np
app = Flask(__name__)

z_entent= C.Function.load("Models/MenuMaker_Intent_model.model")
z_entity = C.Function.load("Models/MenuMaker_Entity_model.model")

word_file = "query_words.wl"
query_file = "queries.wl"
entent_file = "entents.wl"
entity_file = "entities.wl"
punc = string.punctuation

lstm_cells_num = 200
embedding_dim = 100
classify_entents = False
entent_dim = 23
entities_dim = 16
vocab_dim = 616
access_token = "SVHgomCs5K9mOk8bL7As8nwt6RW9GbPoT1o1FImOYNQSAp5CaPAcpaxRds/vH4v20XS9J+ZcbnBEUgSLk5ojM96fKSZ21Y6AsO4RZdWkzfu9Yve/9ePyFQ=="

def process_phrase(phrase):
    result = ""
    for c in phrase:
        if not c in punc:
            result+=c
        else:
            result+=" "
#     if use_stemmer:
#         stem = stemmer.stem(result)
#         return stem.lower()
    else:
        return result.lower()

def process_utterance(query):
    query = process_phrase(query).strip()
    query = "BOS " + query + " EOS"
    in_data = []

    f = open(word_file, "r")
    q_words = f.read().split("\n")
    f.close()

    for word in query.split(" "):
        if word in q_words:
            hot = np.zeros(vocab_dim, dtype=np.float32)
            index = q_words.index(word)
            hot[index] = 1
            in_data.append(hot)
    in_data = np.array(in_data, dtype=np.float32)

    z_softmax = C.softmax(z_entent)
    preds = z_softmax.eval({z_entent.arguments[0]: [in_data]})
    entent_index = np.argmax(preds[0])

    f = open(entent_file, "r")
    entents_arr = f.read().split("\n")
    f.close()

    parsed = []
    in_data = []
    for word in query.split(" "):
        if word in q_words:
            parsed.append(word)
            hot = np.zeros(vocab_dim, dtype=np.float32)
            index = q_words.index(word)
            hot[index] = 1
            in_data.append(hot)
    response = {}
    response['Utterance'] = query
    response['Predicted Intent'] = entents_arr[entent_index]
    # print("Utterance:", query)
    # print("=======================")
    # print("Predicted Entent:", entents_arr[entent_index])
    # print("=======================")
    # print("Predicted Entities:")

    z_softmax = C.softmax(z_entity)
    preds = z_softmax.eval({z_entity.arguments[0]: [in_data]})

    f = open(entity_file, "r")
    entity_arr = f.read().split("\n")
    f.close()
    entities_dict = {}
    count = 0
    for pred in preds[0]:

        ix = np.argmax(pred)
        # print(parsed[count], ":", entity_arr[ix])
        p_c = parsed[count]
        # print(p_c)
        e_a = entity_arr[ix]
        # print(e_a)
        entities_dict[p_c] = e_a
        # print(entities_dict)
        count += 1
    response['Predicted Entities'] = [entities_dict]
    return response


@app.route('/query/<query>', methods = ['GET', 'POST'])
def render_API(query):
    response = [process_utterance(query)]
    # print(response)
    return jsonify({'response' : response})
    # return 'success'

@app.route('/queries',methods = ['GET','POST'])
def render_queries():
    headers = {'access_token':access_token}
    r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_queries', headers=headers)
    # print(r.text)
    return jsonify(r.json())

@app.route('/intents',methods = ['GET','POST'])
def render_intents():
    headers = {'access_token':access_token}
    r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_intents', headers=headers)
    # print(r.text)
    return jsonify(r.json())

@app.route('/tokens',methods = ['GET','POST'])
def render_tokens():
    headers = {'access_token':access_token}
    r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_tokens', headers=headers)
    # print(r.text)
    return jsonify(r.json())

@app.route('/entities',methods = ['GET','POST'])
def render_entities():
    headers = {'access_token':access_token}
    r = requests.get('https://nlp-dataentry.azurewebsites.net/api/data_access/get_entities', headers=headers)
    # print(r.text)
    return jsonify(r.json())




if __name__ == '__main__':
    app.run()