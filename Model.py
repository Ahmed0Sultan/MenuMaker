# coding: utf-8
import sys
import cntk as C
import requests

lstm_cells_num = 200
embedding_dim = 100
classify_entents = False
entent_dim = 27
entities_dim = 22
vocab_dim = 463

def create_reader(path,is_training):
    feature_stream = C.io.StreamDef(field = "S0",shape = vocab_dim,is_sparse = True)
    intent_stream = C.io.StreamDef(field = "S1",shape = entent_dim,is_sparse = True)
    entity_stream = C.io.StreamDef(field = "S2",shape = entities_dim,is_sparse = True)
    deserializer = C.io.CTFDeserializer(path,
     C.io.StreamDefs(features = feature_stream,intents = intent_stream,entities = entity_stream))
    return C.io.MinibatchSource(deserializer,randomize = is_training,
                               max_sweeps = C.io.INFINITELY_REPEAT if is_training else 1)
def create_bi_direction_recurrence(fwd,bwd):
    x = C.placeholder()
    forward = C.layers.Recurrence(fwd, go_backwards = False)
    backward = C.layers.Recurrence(bwd,go_backwards = True)
    return C.splice(forward(x),backward(x))
def create_bi_direction_folded_recurrence(fwd,bwd):
    x = C.placeholder()
    forward = C.layers.Fold(fwd, go_backwards = False)
    backward = C.layers.Fold(bwd,go_backwards = True)
    return C.splice(forward(x),backward(x))
def create_entent_model(features):
    return C.layers.Sequential([
        C.layers.Embedding(embedding_dim),
        C.layers.Stabilizer(),
        create_bi_direction_recurrence(C.layers.LSTM(lstm_cells_num//2),C.layers.LSTM(lstm_cells_num//2)),
        create_bi_direction_folded_recurrence(C.layers.LSTM(lstm_cells_num//2),C.layers.LSTM(lstm_cells_num//2)),
        #C.layers.Fold(C.layers.LSTM(lstm_cells_num),go_backwards = False),
        C.layers.Dropout(0.2),
        C.layers.Dense(entent_dim,activation = None)
    ])(features)
def create_entity_model(features):
    return C.layers.Sequential([
        C.layers.Embedding(embedding_dim),
        C.layers.Stabilizer(),
        #create_bi_direction_recurrence(C.layers.LSTM(lstm_cells_num//2),C.layers.LSTM(lstm_cells_num//2)),
        C.layers.Recurrence(C.layers.LSTM(lstm_cells_num),go_backwards = False),
        C.layers.Dropout(0.2),
        C.layers.Dense(entities_dim,activation = None)
    ])(features)




train_file = "train.ctf"
entent_dim = 27
entities_dim = 22
vocab_dim = 463

classify_entents = False

input = C.sequence.input_variable(vocab_dim)
label = C.input_variable(entent_dim) if classify_entents else C.sequence.input_variable(entities_dim)

z = create_entent_model(input) if classify_entents else create_entity_model(input)
loss = C.cross_entropy_with_softmax(z, label)
error = C.classification_error(z, label)
epochs = 200
epoch_samples_num = 1013
train_minibatches = 1

lr = [0.1] * 5 + [0.05] * 10 + [0.08] * 5
lr_schedule = C.learning_parameter_schedule(lr, epoch_samples_num // 10)
momentum = C.momentum_schedule(0.9)
learner = C.adam(z.parameters, lr=lr_schedule, momentum=momentum)
prog_printer = C.logging.ProgressPrinter(epoch_samples_num // 10)
trainer = C.Trainer(z, (loss, error), [learner], prog_printer)
train_reader = create_reader(train_file, True)

train_input_map = {
    input: train_reader.streams.features,
    label: train_reader.streams.intents
} if classify_entents else {
    input: train_reader.streams.features,
    label: train_reader.streams.entities
}

errors = []
losses = []

C.logging.log_number_of_parameters(z)

for _ in range(0, epochs):
    read_samples = 0
    while read_samples < epoch_samples_num:
        data = train_reader.next_minibatch(min(train_minibatches, epoch_samples_num - read_samples),
                                           input_map=train_input_map)
        try:
            trainer.train_minibatch(data)
        except:
            continue
        read_samples += data[input].num_samples
        errors.append(trainer.previous_minibatch_evaluation_average)
        losses.append(trainer.previous_minibatch_loss_average)

if classify_entents:
    test_reader = create_reader(train_file, False)

    test_map = {
        input: test_reader.streams.features,
        label: test_reader.streams.intents
    }
    test_minibatches_num = 512

    while True:
        data = test_reader.next_minibatch(test_minibatches_num, input_map=test_map)
        if not data:
            break
        trainer.test_minibatch(data)

    trainer.summarize_test_progress()

if classify_entents:
    z.save("Models/MenuMaker_Intent_model.model")
else:
    z.save("Models/MenuMaker_Entity_model.model")